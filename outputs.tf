output "terraform_kms_keyid_name" {
  value       = aws_kms_key.terraform_kms_key.key_id
  description = "The KMS key ID for Terraform."
}

output "terraform_s3_bucket_name" {
  value       = aws_s3_bucket.terraform_s3_bucket.id
  description = "The S3 bucket name for Terraform."
}

output "terraform_dynamodb_table_name" {
  value       = aws_dynamodb_table.terraform_dynamodb_table.name
  description = "The DynamoDB table name for Terraform backend"
}
