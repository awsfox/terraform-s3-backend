# Overview
Terraform codes in the repository create a S3 bucket and a DynamoDB table in AWS, which will be used as Terraform backend.  
# Steps
## 1. Download codes and set up AWS credentials.
Download the codes.  
Set up AWS credentials in local environment. It is normally done via AWS profile or environment variables.  
## 2. Update terraform.tfvars.template file.
Rename terraform.tfvars.template to terraform.tfvars and update variable values.  
## 3. Deploy resources in AWS.
```
terraform init
terraform plan
terraform apply
```
Note down the output values from 'terraform apply' command.
- terraform_dynamodb_table_name
- terraform_kms_keyid_name
- terraform_s3_bucket_name
  
## 4. Update backend.tf.template file.
Rename backend.tf.template to backend.tf and update configurations with actual output values in previous step.  
## 5. Migrate the existing state to the new S3 backend.
Run the following command and confirm 'yes' to migrate state file.
```
terraform init
```
Once successful, remove local state files.
```
rm terraform.tfstate terraform.tfstate.backup
```
  
## 6. Verify the new S3 backend.
Verify the new S3 backend by run Terraform commands.
```
terraform init
terraform plan
terraform apply
```
  