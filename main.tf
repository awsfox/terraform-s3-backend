resource "aws_kms_key" "terraform_kms_key" {
  description = "KMS key used to encrypt S3 bucket and DynamoDB table for Terraform backend."
}

resource "aws_kms_alias" "terraform_kms_key_alias" {
  name          = "alias/${var.org}-${var.project}-terraform-cmk-${var.env}"
  target_key_id = aws_kms_key.terraform_kms_key.key_id
}

resource "aws_s3_bucket" "terraform_s3_bucket" {
  bucket = "${var.org}-${var.project}-terraform-states-${var.env}"
}

# Block public access to the S3 bucket.
resource "aws_s3_bucket_public_access_block" "terraform_s3_bucket_public_access" {
  bucket                  = aws_s3_bucket.terraform_s3_bucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# Enable versioning for the S3 bucket.
resource "aws_s3_bucket_versioning" "terraform_s3_bucket_versioning" {
  bucket = aws_s3_bucket.terraform_s3_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

# Enable encryption for the S3 bucket.
resource "aws_s3_bucket_server_side_encryption_configuration" "terraform_s3_bucket_encryption" {
  bucket = aws_s3_bucket.terraform_s3_bucket.id
  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.terraform_kms_key.arn
      sse_algorithm     = "aws:kms"
    }
  }
}

resource "aws_dynamodb_table" "terraform_dynamodb_table" {
  name         = "${var.org}-${var.project}-terraform-locks-${var.env}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
  server_side_encryption {
    enabled     = true
    kms_key_arn = aws_kms_key.terraform_kms_key.arn
  }
}
