variable "aws_region" {
  type        = string
  description = "AWS region e.g. ap-southeast-2."
}

variable "org" {
  type        = string
  description = "Organisation name."
}

variable "project" {
  type        = string
  description = "Project name."
}

variable "env" {
  type        = string
  description = "Environment name e.g. dev, sit."
}
